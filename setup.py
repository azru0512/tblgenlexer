"""
LLVM Tblgen syntax highlighting for Pygments.
"""

from setuptools import setup

entry_points = """
[pygments.lexers]
tblgen = tblgen.tblgen:TblgenLexer
"""

setup(
    name         = 'tblgen',
    version      = '0.1',
    description  = __doc__,
    author       = "Chen Wei-Ren",
    packages     = ['tblgen'],
    entry_points = entry_points
)
