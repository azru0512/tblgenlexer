# Based on pygments documentation
from __future__ import print_function
from pygments.lexers.compiled import CppLexer
from pygments.token import *

class TblgenLexer(CppLexer):
    """
    For LLVM tblgen
    """

    name = 'Tblgen'
    aliases = ['tblgen']
    filenames = ['*.td']

    tblgen_keywords = [ 'def', 'let', 'include' ]
    tlbgen_type = [ 'bit', 'bits' , 'string' ]

    def get_tokens_unprocessed(self, text):
        for index, token, value in CppLexer.get_tokens_unprocessed(self, text):
            if token is Name and value in self.tblgen_keywords:
                yield index, Keyword, value
            elif token is Name and value in self.tlbgen_type:
                yield index, Keyword.Type, value
            else:
                yield index, token, value

if __name__ == '__main__':
    from pygments import highlight
    from pygments.lexers import PythonLexer
    from pygments.formatters import HtmlFormatter, Terminal256Formatter
    from pygments.formatters import RawTokenFormatter
    from sys import argv

    if len(argv) > 1:
        import io

        for arg in argv[1:]:
            input = io.open(arg, 'r')
            code = input.read(-1)
            print("Highlighting " + arg)
            #print(highlight(code, MyDiffLexer(encoding='chardet'),
            #      HtmlFormatter(encoding='utf-8')))
            print(highlight(code, TblgenLexer(encoding='chardet'),
                  Terminal256Formatter(encoding='utf-8')))

    else:
        code = """
def FeatureFPU : SubtargetFeature<"fpu", "fpu", "true",
    "Enable FPU">;
""";
        # print(highlight(code, MyDiffLexer(), HtmlFormatter()))
        print(highlight(code, TblgenLexer(), Terminal256Formatter()))
