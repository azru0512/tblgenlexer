tblgenlexer
===========

LLVM TableGen Syntax Highlight

You can download and run this extension as below:

    $ git clone git://github.com/azru0512/tblgenlexer.git
    $ cd tblgenlexer
    $ python setup.py bdist_egg;
    $ python setup.py install
